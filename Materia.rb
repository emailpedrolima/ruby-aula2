class Materia
    attr_accessor :ementa, :nome, :professores
    def initialize(ementa, nome, professores)
      
        @nome    = nome
        @ementa = ementa 
        @materias   = []
        
    end

    def adicionarProfessor(nome_professor)
      @materias.push(nome_professor)
    end
end
