class Professor < Usuario 
    attr_accessor :matricula, :materias, :salario
    def initialize(nome, email, senha, nascimento, logado, matricula, materias, salario)
        super(nome: nome, email: email, senha: senha, nascimento: nascimento, logado: logado)
        @matriclula = matricula 
        @materias   = []
        @salario    = salario.to_f
    end

    def inscrever(nome_materia)
      @materias.push(nome_materia)
        
    end
    
end
