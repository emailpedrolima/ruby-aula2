class Aluno < Usuario
    attr_accessor :matricula, :periodo_letivo, :curso, :turmas
    def initialize(nome, email, senha, nascimento,logado, matricula, periodo_letivo,curso,turmas)
        super(nome: nome, email: email, senha: senha, nascimento: nascimento, logado: logado)
        @matricula = matricula
        @periodo_letivo = periodo_letivo
        @curso = curso
        @turmas = []
        

    end

    def inscrever(nome_turma)
      @turmas.push(nome_turma)
        
    end
end